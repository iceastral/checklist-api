﻿namespace CheckList.Domain.Contracts
{
    public interface IUnitOfWork
    {
        int SaveChanges();

        int SaveChanges(bool acceptAllChangesOnSuccess);
    }
}
