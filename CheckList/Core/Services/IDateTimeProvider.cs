﻿namespace CheckList.Core.Services
{
    public interface IDateTimeProvider
    {
        DateTime Now();
    }
}
