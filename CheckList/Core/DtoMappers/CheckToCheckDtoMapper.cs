﻿using CheckList.Core.Dto;
using CheckList.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckList.Core.DtoMappers
{
    internal static class DtoMapper
    {
        public static CheckDto CheckToChekDtoMapper(Check check, bool isChecked)
        {
            return new CheckDto
            {
                Id = check.Id,
                Description = check.Description,
                IsChecked = isChecked
            };
        }

        public static CheckListDto CheckListToCheckListDto(List<Check> checkList, int memberId)
        {
            return new CheckListDto
            {
                TeamMemberId = memberId,
                Checks = checkList.Select(c =>
                {
                    return new CheckDto
                    {
                        Id = c.Id,
                        Description = c.Description,
                        IsChecked = c.TeamMemberCheckList.Any()
                    };
                }).ToList()
            };
        }

        public static MemberResultDto MembersToMembersResultDto(TeamMember teamMember, bool checkListDone)
        {
            return new MemberResultDto
            {
                Id = teamMember.Id,
                Name = teamMember.Name,
                CheckListDone = checkListDone
            };
        }
    }
}
