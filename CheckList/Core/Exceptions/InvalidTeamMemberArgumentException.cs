﻿namespace CheckList.Core.Exceptions
{
    public class InvalidTeamMemberArgumentException : Exception
    {
        public InvalidTeamMemberArgumentException()
        {
        }

        public InvalidTeamMemberArgumentException(string message) : base(message)
        {
        }
    }
}
