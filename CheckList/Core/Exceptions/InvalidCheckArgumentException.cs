﻿namespace CheckList.Core.Exceptions
{
    public class InvalidCheckArgumentException : Exception
    {
        public InvalidCheckArgumentException()
        {
        }

        public InvalidCheckArgumentException(string message) : base(message)
        {
        }
    }
}
