﻿namespace CheckList.Core.Dto
{
    public class MemberResultDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool CheckListDone { get; set; }

        public override bool Equals(object other)
        {
            var toCompareWith = other as MemberResultDto;
            if (toCompareWith == null)
            {
                return false;
            }

            return
                Id == toCompareWith.Id &&
                Name == toCompareWith.Name &&
                CheckListDone == toCompareWith.CheckListDone;
        }
    }
}
