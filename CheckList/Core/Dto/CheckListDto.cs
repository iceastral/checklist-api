﻿namespace CheckList.Core.Dto
{
    public class CheckListDto
    {
        public int TeamMemberId { get; set; }

        public List<CheckDto> Checks { get; set; }
    }
}
