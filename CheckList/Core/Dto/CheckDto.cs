﻿using System;

namespace CheckList.Core.Dto
{
    public class CheckDto
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public bool IsChecked { get; set; }

        public override bool Equals(object other)
        {
            var toCompareWith = other as CheckDto;
            if (toCompareWith == null)
            {
                return false;
            }

            return 
                Id == toCompareWith.Id &&
                Description == toCompareWith.Description &&
                IsChecked == toCompareWith.IsChecked;
        }
    }
}
