﻿using CheckList.Domain.Contracts;

namespace CheckList.Core.Models
{
    public class TeamMemberCheckList : IAggregateRoot
    {
        public int Id { get; set; }

        public DateTime CreatedOn { get; set; }

        public int TeamMemberId { get; set; }

        public int CheckId { get; set; }

        virtual public TeamMember TeamMember { get; set; }

        virtual public Check Check { get; set; }
    }
}
