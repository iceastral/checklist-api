﻿using CheckList.Domain.Contracts;

namespace CheckList.Core.Models
{
    public class TeamMember : IAggregateRoot
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual List<TeamMemberCheckList> TeamMemberCheckList { get; set; }
    }
}
