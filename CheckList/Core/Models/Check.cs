﻿using CheckList.Domain.Contracts;

namespace CheckList.Core.Models
{
    public class Check : IAggregateRoot
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public virtual List<TeamMemberCheckList> TeamMemberCheckList { get; set; }
    }
}
