﻿using CheckList.Domain.Contracts;
using CheckList.Core.Models;

namespace CheckList.Core.CheckList
{
    public interface ITeamMemberRepository : IRepository<TeamMember>
    {
        void Add(Check entity);

        void Remove(Check entity);

        Task AddCheckForTeamMember(TeamMember member, int memberId, int checkId);

        Task<List<Check>> GetCheckListForMemberAsync(int memberId, DateTime dateTime);

        Task<List<Check>> GetCheckListForAllTeamByDateAsync(DateTime dateTime);

        Task<List<TeamMember>> GetMembersAsync();

        Task<TeamMember?> GetTeamMemberCheckListByIdAsync(int memberId);

        Task<TeamMember?> GetTeamMemberByIdAsync(int memberId);

        Task<Check> GetCheckByIdAsync(int checkId);
    }
}
