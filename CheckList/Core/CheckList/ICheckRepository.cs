﻿using CheckList.Domain.Contracts;
using CheckList.Core.Models;

namespace CheckList.Core.CheckList
{
    public interface ICheckRepository : IRepository<Check>
    {
        void Add(Check entity);

        void Remove(Check entity);
    }
}
