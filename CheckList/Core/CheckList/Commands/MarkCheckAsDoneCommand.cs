﻿using MediatR;

namespace Core.CheckList.Commands
{
    public class MarkCheckAsDoneCommand : IRequest<bool>
    {
        public MarkCheckAsDoneCommand(int teamMemberId, int checkId)
        {
            TeamMemberId = teamMemberId;
            CheckId = checkId;
        }

        public int TeamMemberId { get; set; }

        public int CheckId { get; set; }
    }
}
