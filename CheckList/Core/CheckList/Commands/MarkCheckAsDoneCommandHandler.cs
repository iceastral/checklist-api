﻿using CheckList.Core.CheckList;
using CheckList.Core.Exceptions;
using CheckList.Core.Services;
using MediatR;

namespace Core.CheckList.Commands
{
    public class MarkCheckAsDoneCommandHandler : IRequestHandler<MarkCheckAsDoneCommand, bool>
    {
        private readonly ITeamMemberRepository _teamMemberRepository;
        private readonly IDateTimeProvider _dateTimeProvider;

        public MarkCheckAsDoneCommandHandler(ITeamMemberRepository teamMemberRepository, IDateTimeProvider dateTimeProvider)
        {
            _teamMemberRepository = teamMemberRepository;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<bool> Handle(MarkCheckAsDoneCommand request, CancellationToken cancellationToken)
        {
            var member = await _teamMemberRepository.GetTeamMemberCheckListByIdAsync(request.TeamMemberId);
            if (member == null)
            {
                throw new InvalidTeamMemberArgumentException();
            }

            var check = await _teamMemberRepository.GetCheckByIdAsync(request.CheckId);
            if (check == null)
            {
                throw new InvalidCheckArgumentException();
            }

            if (!member.TeamMemberCheckList.Any(tml =>
                tml.CheckId == request.CheckId &&
                tml.TeamMemberId == request.TeamMemberId &&
                tml.CreatedOn.Date == _dateTimeProvider.Now().Date))
            {
                await _teamMemberRepository.AddCheckForTeamMember(member, request.TeamMemberId, request.CheckId);
                _teamMemberRepository.UnitOfWork.SaveChanges();
            }

            return true;
        }
    }
}