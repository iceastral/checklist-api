﻿using CheckList.Core.Dto;
using CheckList.Core.DtoMappers;
using CheckList.Core.Services;
using MediatR;

namespace CheckList.Core.CheckList.Queries
{
    public class GetCheckListForAllTeamQueryHandler : IRequestHandler<GetCheckListForAllTeamQuery, List<MemberResultDto>>
    {
        private readonly ITeamMemberRepository _teamMemberRepository;
        private readonly IDateTimeProvider _dateTimeProvider;

        public GetCheckListForAllTeamQueryHandler(ITeamMemberRepository teamMemberRepository, IDateTimeProvider dateTimeProvider)
        {
            _teamMemberRepository = teamMemberRepository;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<List<MemberResultDto>> Handle(GetCheckListForAllTeamQuery request, CancellationToken cancellationToken)
        {
            var checks = await _teamMemberRepository.GetCheckListForAllTeamByDateAsync(_dateTimeProvider.Now().Date);
            var members = await _teamMemberRepository.GetMembersAsync();

            var result = members.Select(member => 
                {
                    var isMemberCompleteTodayCheckList = checks
                        .All(c => c.TeamMemberCheckList.Any(tml => tml.TeamMemberId == member.Id));

                    return DtoMapper.MembersToMembersResultDto(member, isMemberCompleteTodayCheckList);
                }).ToList();

            return result;
        }
    }
}