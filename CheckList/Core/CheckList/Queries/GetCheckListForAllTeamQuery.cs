﻿using CheckList.Core.Dto;
using MediatR;

namespace CheckList.Core.CheckList.Queries
{
    public class GetCheckListForAllTeamQuery : IRequest<List<MemberResultDto>>
    {
    }
}
