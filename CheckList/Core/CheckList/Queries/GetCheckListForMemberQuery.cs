﻿using CheckList.Core.Dto;
using MediatR;

namespace CheckList.Core.CheckList.Queries
{
    public class GetCheckListForMemberQuery : IRequest<CheckListDto>
    {
        public GetCheckListForMemberQuery(int teamMemberId)
        {
            TeamMemberId = teamMemberId;
        }

        public int TeamMemberId { get; set; }
    }
}
