﻿using CheckList.Core.Dto;
using CheckList.Core.DtoMappers;
using CheckList.Core.Exceptions;
using CheckList.Core.Services;
using MediatR;

namespace CheckList.Core.CheckList.Queries
{
    public class GetCheckListForMemberQueryHandler : IRequestHandler<GetCheckListForMemberQuery, CheckListDto>
    {
        private readonly ITeamMemberRepository _teamMemberRepository;
        private readonly IDateTimeProvider _dateTimeProvider;

        public GetCheckListForMemberQueryHandler(ITeamMemberRepository teamMemberRepository, IDateTimeProvider dateTimeProvider)
        {
            _teamMemberRepository = teamMemberRepository;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<CheckListDto> Handle(GetCheckListForMemberQuery request, CancellationToken cancellationToken)
        {
            var member = await _teamMemberRepository.GetTeamMemberByIdAsync(request.TeamMemberId);
            if (member == null)
            {
                throw new InvalidTeamMemberArgumentException();
            }

            var memberCheckList = await _teamMemberRepository.GetCheckListForMemberAsync(request.TeamMemberId, _dateTimeProvider.Now().Date);

            var result = DtoMapper.CheckListToCheckListDto(memberCheckList, request.TeamMemberId);

            return result;
        }
    }
}
