﻿using CheckList.Core.CheckList.Queries;
using CheckList.Core.CheckList;
using CheckList.Core.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckList.Domain.Contracts;
using CheckList.Core.Models;
using CheckList.Core.Dto;

namespace CheckList.UnitTests.CqrsHandlerTests
{
    [TestFixture]
    internal class GetCheckListForAllTeamQueryHandlerTests
    {
        private Mock<ITeamMemberRepository> _repository;
        private GetCheckListForAllTeamQueryHandler _handler;
        private Mock<IDateTimeProvider> _dateTimeProvider;
        private DateTime _dateTimeNow;

        [SetUp]
        public async Task Initialize()
        {
            _dateTimeNow = new DateTime(2022, 1, 1);
            _repository = new Mock<ITeamMemberRepository>();
            _dateTimeProvider = new Mock<IDateTimeProvider>();
            _dateTimeProvider.Setup(x => x.Now()).Returns(_dateTimeNow);
            _handler = new GetCheckListForAllTeamQueryHandler(_repository.Object, _dateTimeProvider.Object);
            _repository.Setup(x => x.UnitOfWork).Returns(Mock.Of<IUnitOfWork>());
        }

        [Test]
        public async Task GetCheckListForAllTeam_Success()
        {
            var _members = new List<TeamMember>()
            {
                new TeamMember()
                {
                    Id = 17,
                    Name= "Max"
                },
                new TeamMember()
                {
                    Id = 18,
                    Name = "Polina"
                }
            };
            var _checks = new List<Check>()
            {
                new Check()
                {
                    TeamMemberCheckList = new List<TeamMemberCheckList>() 
                    {
                        new TeamMemberCheckList() { TeamMemberId = _members[0].Id },
                        new TeamMemberCheckList() { TeamMemberId = _members[1].Id }
                    }
                },
                new Check()
                {
                    TeamMemberCheckList = new List<TeamMemberCheckList>() {
                        new TeamMemberCheckList() { TeamMemberId = _members[0].Id },
                    }
                }
            };

            _repository.Setup(x => x.GetCheckListForAllTeamByDateAsync(It.IsAny<DateTime>())).Returns(Task.FromResult(_checks));
            _repository.Setup(x => x.GetMembersAsync()).Returns(Task.FromResult(_members));

            var result = await _handler.Handle(new GetCheckListForAllTeamQuery(), default);

            var _expectedResult = new List<MemberResultDto>()
            {
                new MemberResultDto()
                {
                    Id = _members[0].Id,
                    Name = _members[0].Name,
                    CheckListDone = true,
                },
                new MemberResultDto()
                {
                    Id = _members[1].Id,
                    Name = _members[1].Name,
                    CheckListDone = false,
                }
            };

            CollectionAssert.AreEqual(result.ToArray(), _expectedResult.ToArray());
        }
    }
}
