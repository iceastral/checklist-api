﻿using CheckList.Core.CheckList;
using CheckList.Core.CheckList.Queries;
using CheckList.Core.Dto;
using CheckList.Core.Models;
using CheckList.Core.Services;
using CheckList.Domain.Contracts;
using Moq;
using NUnit.Framework;

namespace CheckList.UnitTests.CqrsHandlerTests
{
    [TestFixture]
    internal class GetCheckListForMemberQueryHandlerTests
    {
        private Mock<ITeamMemberRepository> _repository;
        private GetCheckListForMemberQueryHandler _handler;
        private Mock<IDateTimeProvider> _dateTimeProvider;
        private DateTime _dateTimeNow;

        [SetUp]
        public async Task Initialize()
        {
            _dateTimeNow = new DateTime(2022, 1, 1);
            _repository = new Mock<ITeamMemberRepository>();
            _dateTimeProvider = new Mock<IDateTimeProvider>();
            _dateTimeProvider.Setup(x => x.Now()).Returns(_dateTimeNow);
            _handler = new GetCheckListForMemberQueryHandler(_repository.Object, _dateTimeProvider.Object);
            _repository.Setup(x => x.UnitOfWork).Returns(Mock.Of<IUnitOfWork>());
        }

        [Test]
        public async Task GetCheckListForMember_Success()
        {
            var _member = new TeamMember() { Id = 12, Name = "Max", TeamMemberCheckList = new List<TeamMemberCheckList>() };
            var _memberCheckList = new List<Check>()
            {
                new Check()
                {
                    Id = 1,
                    Description = "Some check 1",
                    TeamMemberCheckList = new List<TeamMemberCheckList>() { new TeamMemberCheckList() { Id = 1 } }
                },
                new Check() 
                {
                    Id = 2,
                    Description = "Some check 2",
                    TeamMemberCheckList = new List<TeamMemberCheckList>()
                }
             };
            _repository.Setup(x => x.GetTeamMemberByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(_member));
            _repository.Setup(x => x.GetCheckListForMemberAsync(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(Task.FromResult(_memberCheckList));

            var result = await _handler.Handle(new GetCheckListForMemberQuery(_member.Id), default);

            var expectedResult = new CheckListDto()
            {
                TeamMemberId = 12,
                Checks = new List<CheckDto>() 
                {
                    new CheckDto() 
                    { 
                        Id = _memberCheckList[0].Id,
                        Description = _memberCheckList[0].Description,
                        IsChecked = true 
                    },
                    new CheckDto()
                    {
                        Id = _memberCheckList[1].Id,
                        Description = _memberCheckList[1].Description,
                        IsChecked = false
                    }
                }
            };
            Assert.AreEqual(result.TeamMemberId, expectedResult.TeamMemberId);
            CollectionAssert.AreEqual(result.Checks.ToArray(), expectedResult.Checks.ToArray());
        }

        [Test]
        public async Task GetCheckListForMember_Failed_Member_Not_Found()
        {
            var _memberCheckList = new List<Check>()
            {
                new Check()
                {
                    Id = 1,
                    Description = "Some check 1",
                    TeamMemberCheckList = new List<TeamMemberCheckList>() { new TeamMemberCheckList() { Id = 1 } }
                }
             };

            _repository.Setup(x => x.GetTeamMemberByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(null as TeamMember));
            _repository.Setup(x => x.GetCheckListForMemberAsync(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(Task.FromResult(_memberCheckList));

            Assert.That(async () => await _handler.Handle(new GetCheckListForMemberQuery(1), default), Throws.Exception);
            _repository.Verify(x => x.GetCheckListForMemberAsync(It.IsAny<int>(), It.IsAny<DateTime>()), Times.Never);
        }
    }
}
