﻿using CheckList.Core.CheckList;
using CheckList.Core.Models;
using CheckList.Core.Services;
using CheckList.Domain.Contracts;
using Core.CheckList.Commands;
using Moq;
using NUnit.Framework;

namespace CheckList.UnitTests.CqrsHandlerTests
{
    [TestFixture]
    internal class MarkCheckAsDoneCommandHandlerTests
    {
        private Mock<ITeamMemberRepository> _repository;
        private MarkCheckAsDoneCommandHandler _handler;
        private Mock<IDateTimeProvider> _dateTimeProvider;
        private DateTime _dateTimeNow;

        [SetUp]
        public async Task Initialize()
        {
            _dateTimeNow = new DateTime(2022, 1, 1);
            _repository = new Mock<ITeamMemberRepository>();
            _dateTimeProvider = new Mock<IDateTimeProvider>();
            _dateTimeProvider.Setup(x => x.Now()).Returns(_dateTimeNow);
            _handler = new MarkCheckAsDoneCommandHandler(_repository.Object, _dateTimeProvider.Object);
            _repository.Setup(x => x.AddCheckForTeamMember(It.IsAny<TeamMember>(), It.IsAny<int>(), It.IsAny<int>()));
            _repository.Setup(x => x.UnitOfWork).Returns(Mock.Of<IUnitOfWork>());
        }

        [Test]
        public async Task MarkAsDoneSuccessTest()
        {
            var _member = new TeamMember() { Id = 1, Name = "Max", TeamMemberCheckList = new List<TeamMemberCheckList>()};
            var _check = new Check() { Id = 1, Description = "Some check" };

            _repository.Setup(x => x.GetTeamMemberCheckListByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(_member));
            _repository.Setup(x => x.GetCheckByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(_check));

            await _handler.Handle(new MarkCheckAsDoneCommand(1, 1), default);

            _repository.Verify(x => x.AddCheckForTeamMember(It.IsAny<TeamMember>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _repository.Verify(x => x.UnitOfWork.SaveChanges(), Times.Once);
        }

        [Test]
        public async Task MarkAsDone_Failed_Wrong_MemberId()
        {
            var _check = new Check() { Id = 1, Description = "Some check" };

            _repository.Setup(x => x.GetTeamMemberCheckListByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(null as TeamMember));
            _repository.Setup(x => x.GetCheckByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(_check));

            Assert.That(async () => await _handler.Handle(new MarkCheckAsDoneCommand(1, 1), default), Throws.Exception);

            _repository.Verify(x => x.AddCheckForTeamMember(It.IsAny<TeamMember>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
            _repository.Verify(x => x.UnitOfWork.SaveChanges(), Times.Never);
        }

        [Test]
        public async Task MarkAsDone_Failed_Wrong_CheckId()
        {
            var _member = new TeamMember() { Id = 1, Name = "Max", TeamMemberCheckList = new List<TeamMemberCheckList>() };
            var _check = new Check() { Id = 1, Description = "Some check" };

            _repository.Setup(x => x.GetTeamMemberCheckListByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(_member));
            _repository.Setup(x => x.GetCheckByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(null as Check));

            Assert.That(async () => await _handler.Handle(new MarkCheckAsDoneCommand(1, 1), default), Throws.Exception);

            _repository.Verify(x => x.AddCheckForTeamMember(It.IsAny<TeamMember>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
            _repository.Verify(x => x.UnitOfWork.SaveChanges(), Times.Never);
        }

        [Test]
        public async Task MarkAsDone_Successed_Not_Saved()
        {
            var _member = new TeamMember() 
            {
                Id = 1,
                Name = "Max",
                TeamMemberCheckList = new List<TeamMemberCheckList>()
                {
                    new TeamMemberCheckList(){ Id = 1, CheckId = 1, TeamMemberId = 1, CreatedOn = _dateTimeNow }
                }
            };
            var _check = new Check() { Id = 1, Description = "Some check" };

            _repository.Setup(x => x.GetTeamMemberCheckListByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(_member));
            _repository.Setup(x => x.GetCheckByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(_check));

            await _handler.Handle(new MarkCheckAsDoneCommand(1, 1), default);

            _repository.Verify(x => x.AddCheckForTeamMember(It.IsAny<TeamMember>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
            _repository.Verify(x => x.UnitOfWork.SaveChanges(), Times.Never);
        }
    }
}
