﻿using CheckList.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckList.DataAccess.Mappings
{
    public class CheckMapping : IEntityTypeConfiguration<Check>
    {
        public void Configure(EntityTypeBuilder<Check> builder)
        {
            builder.ToTable("Check");

            builder.HasIndex(e => e.Id);

            builder.Property(e => e.Id).ValueGeneratedOnAdd();
            builder.Property(e => e.Description).IsRequired();

            builder.HasMany(e => e.TeamMemberCheckList)
                .WithOne(e => e.Check)
                .HasForeignKey(e => e.CheckId)
                .HasConstraintName("FK_TeamMemberChecList_Check_CheckId")
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
