﻿using CheckList.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckList.DataAccess.Mappings
{
    internal class TeamMemberMapping : IEntityTypeConfiguration<TeamMember>
    {
        public void Configure(EntityTypeBuilder<TeamMember> builder)
        {
            builder.ToTable("TeamMember");

            builder.HasIndex(e => e.Id);

            builder.Property(e => e.Id).ValueGeneratedOnAdd();
            builder.Property(e => e.Name).IsRequired();

            builder.HasMany(e => e.TeamMemberCheckList)
                .WithOne(e => e.TeamMember)
                .HasForeignKey(e => e.TeamMemberId)
                .HasConstraintName("FK_TeamMemberChecList_TeamMember_TeamMemberId")
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
