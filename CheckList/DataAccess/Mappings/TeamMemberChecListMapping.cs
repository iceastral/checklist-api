﻿using CheckList.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckList.DataAccess.Mappings
{
    internal class TeamMemberChecListMapping : IEntityTypeConfiguration<TeamMemberCheckList>
    {
        public void Configure(EntityTypeBuilder<TeamMemberCheckList> builder)
        {
            builder.ToTable("TeamMemberCheckList");

            builder.HasIndex(e => e.Id);

            builder.Property(e => e.Id).ValueGeneratedOnAdd();
            builder.Property(e => e.TeamMemberId).IsRequired().HasColumnName("TeamMemberId");
            builder.Property(e => e.CheckId).IsRequired().HasColumnName("CheckId");

            builder.HasOne(d => d.TeamMember)
                .WithMany(d => d.TeamMemberCheckList)
                .HasForeignKey(d => d.TeamMemberId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(d => d.Check)
                .WithMany(d => d.TeamMemberCheckList)
                .HasForeignKey(d => d.CheckId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
