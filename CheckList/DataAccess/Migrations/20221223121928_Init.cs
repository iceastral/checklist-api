﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CheckList.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Check",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Check", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TeamMember",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeamMember", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TeamMemberCheckList",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TeamMemberId = table.Column<int>(type: "int", nullable: false),
                    CheckId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeamMemberCheckList", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TeamMemberChecList_Check_CheckId",
                        column: x => x.CheckId,
                        principalTable: "Check",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TeamMemberChecList_TeamMember_TeamMemberId",
                        column: x => x.TeamMemberId,
                        principalTable: "TeamMember",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Check_Id",
                table: "Check",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMember_Id",
                table: "TeamMember",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMemberCheckList_CheckId",
                table: "TeamMemberCheckList",
                column: "CheckId");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMemberCheckList_Id",
                table: "TeamMemberCheckList",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMemberCheckList_TeamMemberId",
                table: "TeamMemberCheckList",
                column: "TeamMemberId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TeamMemberCheckList");

            migrationBuilder.DropTable(
                name: "Check");

            migrationBuilder.DropTable(
                name: "TeamMember");
        }
    }
}
