﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CheckList.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class FillinDB : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[TeamMember] ([Name]) VALUES
                   ('Max'),
		           ('Sergey'),
		           ('Eduard'),
		           ('Anya'),
		           ('Diana')
    
                INSERT INTO [dbo].[Check] ([Description])  VALUES
                   ('Drink a cup of coffee'),
		           ('Revise TikTok feed'),
		           ('Discuss yesterday''s football match with teammate')
            ");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
