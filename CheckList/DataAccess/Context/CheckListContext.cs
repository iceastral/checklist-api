﻿using Microsoft.EntityFrameworkCore;
using CheckList.Domain.Contracts;
using CheckList.Core.Models;

namespace DataAccess.Context
{
    public class CheckListContext : DbContext, IUnitOfWork
    {
        public CheckListContext (DbContextOptions<CheckListContext> options)
            : base(options)
        {
        }

        public DbSet<TeamMember> TeamMember { get; set; }

        public DbSet<Check> Check { get; set; }

        public DbSet<TeamMemberCheckList> TeamMemberChecList { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);

            base.OnModelCreating(modelBuilder);
        }
    }
}
