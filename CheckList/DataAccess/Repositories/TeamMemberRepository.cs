﻿using CheckList.Core.CheckList;
using CheckList.Core.Models;
using CheckList.Domain.Contracts;
using DataAccess.Context;
using Microsoft.EntityFrameworkCore;

namespace CheckList.DataAccess.Repositories
{
    public class TeamMemberRepository : ITeamMemberRepository
    {
        private readonly CheckListContext _context;
        public IUnitOfWork UnitOfWork => _context;

        public TeamMemberRepository(CheckListContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Add(Check entity)
        {
            throw new NotImplementedException();
        }

        public async Task AddCheckForTeamMember(TeamMember member, int memberId, int checkId)
        {
            member.TeamMemberCheckList.Add(new TeamMemberCheckList()
            {
                CreatedOn = DateTime.Now,
                CheckId = checkId,
                TeamMemberId = memberId
            });
        }

        public async Task<List<Check>> GetCheckListForMemberAsync(int memberId, DateTime dateTime)
        {
            var checks = await _context.Check
                .Include(t => t.TeamMemberCheckList.Where(tml => tml.TeamMemberId == memberId && 
                                                                 tml.CreatedOn.Date == dateTime.Date))
                .ToListAsync();

            return checks;
        }

        public async Task<List<Check>> GetCheckListForAllTeamByDateAsync(DateTime dateTime)
        {
            var checks = await _context.Check
                .Include(t => t.TeamMemberCheckList.Where(tml => tml.CreatedOn.Date == dateTime.Date))
                .ToListAsync();

            return checks;
        }

        public async Task<List<TeamMember>> GetMembersAsync()
        {
            var members = await _context.TeamMember.ToListAsync();

            return members;
        }

        public async Task<TeamMember?> GetTeamMemberCheckListByIdAsync(int memberId)
        {
            var member = await _context.TeamMember.Include(m => m.TeamMemberCheckList).FirstOrDefaultAsync(m => m.Id == memberId);

            return member;
        }

        public async Task<TeamMember?> GetTeamMemberByIdAsync(int memberId)
        {
            var member = await _context.TeamMember.FirstOrDefaultAsync(m => m.Id == memberId);

            return member;
        }

        public async Task<Check> GetCheckByIdAsync(int checkId)
        {
            var check = await _context.Check.FirstOrDefaultAsync(x => x.Id == checkId);

            return check;
        }

        public void Remove(Check entity)
        {
            throw new NotImplementedException();
        }
    }
}
