﻿using Microsoft.Build.Framework;

namespace CheckList.Api.RequestModels
{
    public class MarkAsDoneRequest
    {
        [Required]
        public int TeamMemberId { get; set; }

        [Required]
        public int CheckId { get; set; }
    }
}
