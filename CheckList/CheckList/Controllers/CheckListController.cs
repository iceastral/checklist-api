﻿using Microsoft.AspNetCore.Mvc;
using MediatR;
using Core.CheckList.Commands;
using CheckList.Api.RequestModels;
using CheckList.Core.Models;
using CheckList.Core.CheckList.Queries;
using System.ComponentModel.DataAnnotations;

namespace CheckList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckListController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CheckListController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamMember>>> Get()
        {
            var result = await _mediator.Send(new GetCheckListForAllTeamQuery());

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamMember>> Get([Required]int id)
        {
            var result = await _mediator.Send(new GetCheckListForMemberQuery(id));

            return Ok(result);
        }        

        [HttpPost("MarkAsDone")]
        public async Task<ActionResult> MarkAsDone([FromBody] MarkAsDoneRequest markAsDoneRequest)
        {
            var result = await _mediator.Send(new MarkCheckAsDoneCommand(markAsDoneRequest.TeamMemberId, markAsDoneRequest.CheckId));

            return Ok(result);
        }
    }
}
