﻿using Microsoft.EntityFrameworkCore;
using DataAccess.Context;
using Microsoft.Extensions.DependencyInjection.Extensions;
using CheckList.Core.CheckList;
using CheckList.DataAccess.Repositories;
using Core.CheckList.Commands;
using System.Reflection;
using MediatR;
using CheckList.Core.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<CheckListContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("CheckListContext") ?? throw new InvalidOperationException("Connection string 'CheckListContext' not found.")));

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.TryAddTransient<ITeamMemberRepository, TeamMemberRepository>();
builder.Services.TryAddTransient<IDateTimeProvider, DateTimeProvider>();
builder.Services.AddMediatR(typeof(MarkCheckAsDoneCommand).GetTypeInfo().Assembly);

builder.Services.AddCors(x =>
{
    x.AddPolicy("*",
        c =>
            c.AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
    );
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("*");

app.UseAuthorization();

app.MapControllers();

app.Run();
